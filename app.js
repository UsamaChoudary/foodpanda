const express = require('express');
const app = express();
//const http = require('http').Server(app);
const db = require('./db');

const Products = require('./routes/products');
const Restaurants = require('./routes/restaurants');
const Orders = require('./routes/orders');
const Customers = require('./routes/customers');


app.use('/products',Products);
app.use('/restaurants',Restaurants);
app.use('/orders',Orders);
app.use('/customers',Customers);

//app.use(express.json());
//app.use(bodyparser.json());


app.listen(process.env.PORT || 3000,()=>console.log("Express is runing at 3000"));

module.exports = app;