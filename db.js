const mysql = require('mysql');

const connection = mysql.createConnection
({
    host: 'localhost',
    user: 'root',
    password: '12345',
    database: 'foodpanda'
});

connection.connect(function(err)
{
    if(err) 
    {
        console.log('Following Error Occured During Connecting To DataBase : ' + err);
    }
    else
    {
        console.log("Connected ! ");
    }
});


module.exports=connection;