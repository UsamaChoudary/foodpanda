var express = require('express');
var router = express.Router();
var db = require('../db');
var bodyParser = require('body-parser');
// const Joi = require('joi');

 router.use(bodyParser.json()); // for parsing application/json
// router.use(bodyParser.urlencoded({extended: false})); // for parsing application/x-www-form-urlencoded

/* get method for fetch all products. */

//  App.js   /customers
router.get('/all', function(req, res) 
{
  var sql = "SELECT * FROM customers";
  db.query(sql, function(err, rows) 
  {
    if (err) 
    {
      res.status(500).send('Following Error Occured : ' + err);
      console.log('Following Error Occured : ' + err);
    }
    else
    {
      console.log('Products Displayed!');
      res.json(rows);
    }
  });
});

         /*Creating a new Product*/
         //  app.js  /products
router.post('/create', function(req, res) 
{
    var id = 0;
    const Name = req.body.Name;
    const Contact = req.body.Contact;
    const Email = req.body.Email;
    const Address = req.body.Address;
    const sql = `INSERT INTO customers
    VALUES ('${id}','${Name}','${Contact}','${Email}','${Address}')`;
    db.query(sql, function(err) 
    {
      if(err) 
      {
        res.status(500).send('Following Error Occured : ' + err);
        console.log('Following Error Occured : ' + err);
      }
      else
      {
        res.send('Customer Registered');
        console.log('Customer Registered');
      }
    });
});

router.delete('/delete/:id', function(req,res)
{
  const id = req.params.id;
  const sql = `DELETE FROM customers WHERE Id =${id};`
  db.query(sql,function(err)
  {
    if(err)
    {
      console.log('Following Error Occured : ' + err);
      res.send('Following Error Occurred : ' + err);
    }
    else
    {
      console.log('Deleted!');
      res.send('Deleted!');
    }
  })
});

router.put('/update/:id', function(req,res)
{
  const id = req.params.id;
  const Name = req.body.Name;
  const Contact = req.body.Contact;
  const Email = req.body.Email;
  const Address = req.body.Address;

  const sql =`UPDATE customers
              SET Name = '${Name}', Contact = '${Contact}',
              Email='${Email}', Address = '${Address}'
              WHERE Id = ${id};`
 
  db.query(sql,function(err)
  {
    if(err)
    {
      console.log('Following Error Occured : ' + err);
      res.send('Following Error Occured : ' + err);
    }
    else
    {
      console.log('Updated!');
      res.send('Updated!');
    }
  });

});


module.exports = router;
  