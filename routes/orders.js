var express = require('express');
var router = express.Router();
var db = require('../db');
var bodyParser = require('body-parser');


 router.use(bodyParser.json()); // for parsing application/json
// router.use(bodyParser.urlencoded({extended: false})); // for parsing application/x-www-form-urlencoded

/* get method for fetch all products. */

//  App.js   /orders
router.get('/all', function(req, res) 
{
  var sql = "SELECT * FROM orders";
  db.query(sql, function(err, rows) 
  {
    if (err) 
    {
      res.status(500).send('Following Error Occured : ' + err);
      console.log('Following Error Occured : ' + err);
    }
    else
    {
      console.log('Products Displayed!');
      res.json(rows);
    }
  });
});

         /*Creating a new Product*/
         //  app.js  /products
router.post('/create', function(req, res) 
{
    var id = 0;
    const Customer_Id = req.body.Customer_Id;
    const Date = req.body.Date;
    const Location = req.body.Location;
    const Restaurant_Id = req.body.Restaurant_Id;
    const sql = `INSERT INTO orders 
    VALUES ('${id}','${Customer_Id}','${Date}','${Location}','${Restaurant_Id}')`;
    db.query(sql, function(err) 
    {
      if(err) 
      {
        res.status(500).send('Following Error Occured : ' + err);
        console.log('Following Error Occured : ' + err);
      }
      else
      {
        res.send('Order Placed');
        console.log('Order Placed');
      }
    });
});

router.delete('/delete/:id', function(req,res)
{
  const id = req.params.id;
  const sql = `DELETE FROM orders WHERE Id =${id};`
  db.query(sql,function(err)
  {
    if(err)
    {
      console.log('Following Error Occured : ' + err);
      res.send('Following Error Occurred : ' + err);
    }
    else
    {
      console.log('Deleted!');
      res.send('Deleted!');
    }
  })
});

router.put('/update/:id', function(req,res)
{
  const id = req.params.id;
  const Customer_Id = req.body.Customer_Id;
  const Date = req.body.Date;
  const Location = req.body.Location;
  const Restaurant_Id = req.body.Restaurant_Id;

  const sql =`UPDATE orders
              SET Customer_Id = '${Customer_Id}', Date = '${Date}',
              Location = '${Location}', Restaurant_Id = '${Restaurant_Id}'
              WHERE Id = ${id};`
 
  db.query(sql,function(err)
  {
    if(err)
    {
      console.log('Following Error Occured : ' + err);
      res.send('Following Error Occured : ' + err);
    }
    else
    {
      console.log('Updated!');
      res.send('Updated!');
    }
  });

});


module.exports = router;
  