var express = require('express');
var router = express.Router();
var db = require('../db');
var bodyParser = require('body-parser');


 router.use(bodyParser.json()); // for parsing application/json
// router.use(bodyParser.urlencoded({extended: false})); // for parsing application/x-www-form-urlencoded

/* get method for fetch all products. */

//  App.js   /products
router.get('/all', function(req, res) 
{
  var sql = "SELECT * FROM products";
  db.query(sql, function(err, rows) 
  {
    if (err) 
    {
      res.status(500).send('Following Error Occured : ' + err);
      console.log('Following Error Occured : ' + err);
    }
    else
    {
      console.log('Products Displayed!');
      res.json(rows);
    }
  });
});

         /*Creating a new Product*/
         //  app.js  /products
router.post('/create', function(req, res) 
{
    var id = 0;
    const Name = req.body.Name;
    const Price = req.body.Price;
    const Description = req.body.Description;
    const Restaurant_Id = req.body.Restaurant_Id;
    const sql = `INSERT INTO products 
    VALUES ('${id}','${Name}','${Description}','${Price}','${Restaurant_Id}')`;
    db.query(sql, function(err) 
    {
      if(err) 
      {
        res.status(500).send('Following Error Occured : ' + err);
        console.log('Following Error Occured : ' + err);
      }
      else
      {
        res.send('Product Created');
        console.log('Product Created');
      }
    });
});

router.delete('/delete/:id', function(req,res)
{
  const id = req.params.id;
  const sql = `DELETE FROM products WHERE Id =${id};`
  db.query(sql,function(err)
  {
    if(err)
    {
      console.log('Following Error Occured : ' + err);
      res.send('Following Error Occurred : ' + err);
    }
    else
    {
      console.log('Deleted!');
      res.send('Deleted!');
    }
  })
});

router.put('/update/:id', function(req,res)
{
  const id = req.params.id;
  const name = req.body.name;
  const description = req.body.description;
  const price = req.body.price;
  const rid = req.body.rid;

  const sql =`UPDATE products
              SET Name = '${name}', Description = '${description}',
              Price='${price}', Restaurant_Id = '${rid}'
              WHERE Id = ${id};`
 
  db.query(sql,function(err)
  {
    if(err)
    {
      console.log('Following Error Occured : ' + err);
      res.send('Following Error Occured : ' + err);
    }
    else
    {
      console.log('Updated!');
      res.send('Updated!');
    }
  });

});


module.exports = router;
  