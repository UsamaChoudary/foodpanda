var express = require('express');
var router = express.Router();
var db = require('../db');
var bodyParser = require('body-parser');


 router.use(bodyParser.json()); // for parsing application/json
// router.use(bodyParser.urlencoded({extended: false})); // for parsing application/x-www-form-urlencoded

/* get method for fetch all products. */

//  App.js   /Products
router.get('/all', function(req, res) 
{
  var sql = "SELECT * FROM restaurants";
  db.query(sql, function(err, rows) 
  {
    if (err) 
    {
      res.status(500).send('Following Error Occured : ' + err);
      console.log('Following Error Occured : ' + err);
    }
    else
    {
      console.log('Products Displayed!');
      res.json(rows);
    }
  });
});

         /*Creating a new Product*/
         //  app.js  /products
router.post('/create', function(req, res) 
{
    var id = 0;
    const Name = req.body.Name;
    const Owner = req.body.Owner;
    const Image = req.body.Image;
    const Items = req.body.Items;
    const Delivery_Fee = req.body.Delivery_Fee;
    const Delivery_Timing = req.body.Delivery_Timing;
    const Minimum_Delivery = req.body.Minimum_Delivery;
    const sql = `INSERT INTO restaurants 
    VALUES ('${id}','${Name}','${Owner}','${Image}','${Items}','${Delivery_Fee}','${Delivery_Timing}',
            '${Minimum_Delivery}')`;
    db.query(sql, function(err) 
    {
      if(err) 
      {
        res.status(500).send('Following Error Occured : ' + err);
        console.log('Following Error Occured : ' + err);
      }
      else
      {
        res.send('Restaurant Created');
        console.log('Restaurant Created');
      }
    });
});

router.delete('/delete/:id', function(req,res)
{
  const id = req.params.id;
  const sql = `DELETE FROM restaurants WHERE Id =${id};`
  db.query(sql,function(err)
  {
    if(err)
    {
      console.log('Following Error Occured : ' + err);
      res.send('Following Error Occurred : ' + err);
    }
    else
    {
      console.log('Deleted!');
      res.send('Deleted!');
    }
  })
});

router.put('/update/:id', function(req,res)
{
  const id = req.params.id;
  const Name = req.body.Name;
  const Owner = req.body.Owner;
  const Image = req.body.Image;
  const Items = req.body.Items;
  const Delivery_Fee = req.body.Delivery_Fee;
  const Delivery_Timing = req.body.Delivery_Timing;
  const Minimum_Delivery = req.body.Minimum_Delivery;
  const sql =`UPDATE restaurants
              SET Name = '${Name}', Owner = '${Owner}',
              Image='${Image}', Items = '${Items}',Delivery_Fee='${Delivery_Fee}',
              Delivery_Timing = '${Delivery_Timing}', Minimum_Delivery = '${Minimum_Delivery}'
              WHERE Id = ${id};`
 
  db.query(sql,function(err)
  {
    if(err)
    {
      console.log('Following Error Occured : ' + err);
      res.send('Following Error Occured : ' + err);
    }
    else
    {
      console.log('Updated!');
      res.send('Updated!');
    }
  });

});


module.exports = router;
  